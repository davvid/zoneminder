Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ZoneMinder
Upstream-Contact: Philip Coombes <philip.coombes@zoneminder.com>
Source: https://github.com/ZoneMinder/ZoneMinder
Comment:
 This package was originally debianized by matrix <matrix@cecilia>
 on Mon,  7 Mar 2005 02:07:57 -0500.
 It was re-done for submission to the Debian project by Peter Howard
 <pjh@northern-ridge.com.au> on Fri, 8 Dec 2006 10:19:43 +1100
 Regarding exclusion of non-free "onvif/wsdl" files see
  https://github.com/ZoneMinder/zoneminder/issues/1272
Files-Excluded:
    onvif/wsdl/*.wsdl
    onvif/wsdl/*.xsd

Files: *
Copyright:
    2001-2022 ZoneMinder LLC
    2008      Brian Rudy <brudyNO@SPAMpraecogito.com>
    2001-2010 Chris Kistner
    2003-2008 Corey DeLasaux
    2012      Emmanuel Roullit <emmanuel.roullit@gmail.com>
    2014      Jan M. Hochstein
    2014      Juan Manuel Castro
    2001-2014 Philip Coombes <philip.coombes@zoneminder.com>
    2015      Robin Därmann
    2016      Terry Sanders
    2013      Tim Craig <timcraigNO@SPAMsonic.net>
    2015-2018 Habib Kamei
    2017      Ognyan Bankov
    2017      Martin Gutenbrunner <martin.gutenbrunner@SPAMsonic.net>
    2016      Chris Swertfeger
              Roman Dissertori
License: GPL-2+

Files:
    scripts/ZoneMinder/lib/ZoneMinder/Control/DCS5020L.pm
Copyright:
    2013 Art Scheel
License: GPL-2+ or LGPL-3
Comment: https://github.com/ZoneMinder/zoneminder/issues/2883

Files:
    scripts/ZoneMinder/lib/ZoneMinder/Base.pm.in
    scripts/ZoneMinder/lib/ZoneMinder/ConfigData.pm.in
    scripts/ZoneMinder/lib/ZoneMinder/Config.pm.in
    scripts/ZoneMinder/lib/ZoneMinder/Control/3S.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/AxisV2.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/FI8608W_Y2k.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/FI8620_Y2k.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/FI8918W.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/FI9821W_Y2k.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/FI9831W.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/IPCC7210W.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/M8640.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/mjpgStreamer.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/Ncs370.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/PanasonicIP.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/PelcoD.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/PelcoP.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/SkyIPCam7xx.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/Sony.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/SPP1802SWPTZ.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/Toshiba_IK_WB11A.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/Visca.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/Vivotek_ePTZ.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/WanscamHW0025.pm
    scripts/ZoneMinder/lib/ZoneMinder/Control/Wanscam.pm
    scripts/ZoneMinder/lib/ZoneMinder/Database.pm
    scripts/ZoneMinder/lib/ZoneMinder/Event.pm
    scripts/ZoneMinder/lib/ZoneMinder/Filter.pm
    scripts/ZoneMinder/lib/ZoneMinder/General.pm
    scripts/ZoneMinder/lib/ZoneMinder/Logger.pm
    scripts/ZoneMinder/lib/ZoneMinder/Memory.pm.in
    scripts/ZoneMinder/lib/ZoneMinder.pm
    scripts/ZoneMinder/lib/ZoneMinder/Server.pm
    scripts/ZoneMinder/lib/ZoneMinder/Trigger/Channel/*
    scripts/ZoneMinder/lib/ZoneMinder/Trigger/Channel.pm
    scripts/ZoneMinder/lib/ZoneMinder/Trigger/Connection/*
    scripts/ZoneMinder/lib/ZoneMinder/Trigger/Connection.pm
Copyright:
    2015      Bobby Billingsley
    2008      Brian Rudy
    2008      Brian Rudy <brudyNO@SPAMpraecogito.com>
    2001-2015 Florian Neumair
    2013-2014 Iman Darabi <iman.darabi@gmail.com>
    2014      Juan Manuel Castro
    2001-2008,2014 Philip Coombes
    2015      Robin Daermann
    2013      Tim Craig
    2013      Tim Craig <timcraigNO@SPAMsonic.net>
License: Artistic-1.0-Perl or GPL-2+
Comment: https://github.com/ZoneMinder/ZoneMinder/issues/817

Files:
    src/zm_packet.cpp
    src/zm_packet.h
    src/zm_packetqueue.cpp
    src/zm_packetqueue.h
Copyright:
    2017-2021 ZoneMinder LLC
    2016      Steve Gilvarry
License: GPL-3+

Files:
    dep/jwt-cpp/*
Copyright:
    2018 Dominik Thalhammer
License: Expat

Files:
    dep/jwt-cpp/include/nlohmann/json.hpp
Copyright:
    2013-2019 Niels Lohmann <http://nlohmann.me>
License: Expat

Files:
    dep/jwt-cpp/include/picojson/picojson.h
Copyright:
    2009-2010 Cybozu Labs, Inc.
    2011-2014 Kazuho Oku
License: BSD-2-Clause

Files:
    dep/libbcrypt/*
Copyright:
    2015 trusch
    2014 webvariants GmbH, http://www.webvariants.de
License: Expat

Files:
    dep/libbcrypt/include/bcrypt/bcrypt.h
    dep/libbcrypt/src/bcrypt.c
Copyright: N/A
License: CC0-1.0
Comment:
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see
 <http://creativecommons.org/publicdomain/zero/1.0/>.

Files:
    dep/libbcrypt/include/bcrypt/crypt.h
    dep/libbcrypt/include/bcrypt/crypt_blowfish.h
    dep/libbcrypt/include/bcrypt/crypt_gensalt.h
    dep/libbcrypt/include/bcrypt/ow-crypt.h
    dep/libbcrypt/src/crypt_blowfish.c
    dep/libbcrypt/src/crypt_gensalt.c
    dep/libbcrypt/src/wrapper.c
    dep/libbcrypt/src/x86.S
Copyright:
    2000-2014 Solar Designer <solar@openwall.com>
License: Public-Domain~Solar_Designer
 No copyright is claimed, and the software is hereby placed in the public
 domain.  In case this attempt to disclaim copyright and place the software
 in the public domain is deemed null and void, then the software is
 Copyright (c) 2000-2002 Solar Designer and it is hereby released to the
 general public under the following terms:
 .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted.
 .
  There's ABSOLUTELY NO WARRANTY, express or implied.

Files:
    dep/span-lite/*
Copyright:
    2018-2020 Martin Moene
License: BSL-1.0

Files: distros/*
Copyright:
    2015      Dmitry Smirnov <onlyjob@debian.org>
    2014-2015 Emmanuel Papin <manupap01@gmail.com>
    2014-2015 Isaac Connor <iconnor@connortechnology.com>
    2001-2008 Philip Coombes <philip.coombes@zoneminder.com>
    2005      Serg Oskin
License: GPL-2+

Files:
    cmake/Modules/CheckPrototypeDefinition.cmake
    cmake/Modules/CheckPrototypeDefinition_fixed.cmake
    cmake/Modules/FindGLIB2.cmake
    cmake/Modules/FindPolkit.cmake
    cmake/Modules/GNUInstallDirs.cmake
Copyright:
    2010-2011 Andreas Schneider <asn@cryptomilk.org>
    2009      Dario Freddi <drf@kde.org>
    2005-2011 Kitware, Inc.
    2008      Laurent Montel <montel@kde.org>
    2011      Nikita Krupen'ko <krnekit@gmail.com>
License: BSD-3-clause

Files:
    cmake/Modules/FindPerlModules.cmake
Copyright:
    2012      Iowa State University
License: BSL-1.0

Files: web/api/*
Copyright:
    2005-2021 Cake Software Foundation, Inc. (http://cakefoundation.org)
License: Expat

Files:
    web/includes/csrf/*
Copyright: 2008-2013, Edward Z. Yang
License: BSD-2-Clause

Files:
    web/skins/classic/js/chosen/*
Copyright:
    2011-2017 Harvest http://getharvest.com
License: Expat

Files:
    web/skins/classic/js/bootstrap.*
Copyright:
    2011-2015 Twitter, Inc.
License: Expat

Files:
    web/skins/classic/js/dateTimePicker/jquery-ui-timepicker-addon.*
Copyright:
    2016 Trent Richardson
License: Expat

Files:
    web/skins/classic/js/moment.*
Copyright:
    JS Foundation and other contributors
License: Expat

Files:
    web/skins/classic/js/video.js
Copyright: Brightcove, Inc.
License: Apache-2.0

Files:
    web/skins/classic/js/font/vjs.*
Copyright:
    2013 Brightcove, Inc.
License: Apache-2.0
Comment:
 https://github.com/videojs/video.js-component/tree/master/font

Files:
    web/fonts/MaterialIcons*
Copyright:
    2015 Google, Inc.
License: Apache-2.0
Comment:
 https://github.com/google/material-design-icons

Files:
    web/fonts/glyphicons*
Copyright:
    2014      Jan Kovarik
    2016-2018 Nicolas Mora <mail@babelouest.org>
License: GPL-3

Files:
    Crud/*
    web/api/app/Plugin/Crud/*
Copyright:
    2013 Christian "Jippi" Winther
License: Expat

Files:
    RtspServer/*
Copyright:
    2018 PHZ
License: Expat

Files:
    RtspServer/src/3rdpart/md5/md5.hpp
Copyright: 1999, 2002 Aladdin Enterprises.
License: BSD-3-Aladdin
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
 .
  L. Peter Deutsch
  ghost@aladdin.com

Files:
    CakePHP-Enum-Behavior/*
Copyright:
    2013 Pierre Aboucaya
License: Expat

Files: web/api/app/vendor/composer/installers/*
Copyright:
    2012 Kyle Robinson Young
         Nils Adermann <naderman@naderman.de>
         Jordi Boggiano <j.boggiano@seld.be>
License: Expat

Files:
    web/skins/*/js/jquery-*
Copyright:
    2005-2014 jQuery Foundation, Inc. and other contributors
License: Expat
Comment:
 Includes Sizzle.js http://sizzlejs.com/

Files: web/tools/mootools/*.js
Copyright:
    2006-2015 Valerio Proietti (http://mad4milk.net/)
License: Expat

Files:
    web/vendor/composer/*
Copyright:
    2016 Nils Adermann <naderman@naderman.de>
    2016 Jordi Boggiano <j.boggiano@seld.be>
License: Expat

Files:
    web/vendor/firebase/php-jwt/*
Copyright:
    2011, Neuman Vong
License: BSD-3-clause~Neuman_Vong
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
 .
    * Neither the name of Neuman Vong nor the names of other
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files:
    web/vendor/ircmaxell/password-compat/*
Copyright:
    2012 Anthony Ferrara
License: Expat

Files: debian/*
Copyright:
    2015-2023 Dmitry Smirnov <onlyjob@debian.org>
    2007-2014 Peter Howard <pjh@northern-ridge.com.au>
    2010-2012 Vagrant Cascadian <vagrant@debian.org>
    2001-2008 Philip Coombes <philip.coombes@zoneminder.com>
License: GPL-2+

License: Artistic-1.0-Perl
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License.
 ․
 On Debian systems, the complete text of the Artistic License
 can be found in "/usr/share/common-licenses/Artistic".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
  * The names of Kitware, Inc., the Insight Consortium, or the names of
    any consortium members, or of any contributors, may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSL-1.0
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 ․
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 ․
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 The complete text of the GNU General Public License version 2
 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-3
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 3 of the License.
 ․
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU Lesser General Public
 License Version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 ․
    http://www.apache.org/licenses/LICENSE-2.0
 ․
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ․
 On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in "/usr/share/common-licenses/Apache-2.0".

License: CC0-1.0
 Creative Commons Legal Code
 ․
 CC0 1.0 Universal
 ․
    CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
    LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
    ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
    INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
    REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
    PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
    THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
    HEREUNDER.
 ․
 Statement of Purpose
 ․
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator
 and subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 ․
 Certain owners wish to permanently relinquish those rights to a Work for
 the purpose of contributing to a commons of creative, cultural and
 scientific works ("Commons") that the public can reliably and without fear
 of later claims of infringement build upon, modify, incorporate in other
 works, reuse and redistribute as freely as possible in any form whatsoever
 and for any purposes, including without limitation commercial purposes.
 These owners may contribute to the Commons to promote the ideal of a free
 culture and the further production of creative, cultural and scientific
 works, or to gain reputation or greater distribution for their Work in
 part through the use and efforts of others.
 ․
 For these and/or other purposes and motivations, and without any
 expectation of additional consideration or compensation, the person
 associating CC0 with a Work (the "Affirmer"), to the extent that he or she
 is an owner of Copyright and Related Rights in the Work, voluntarily
 elects to apply CC0 to the Work and publicly distribute the Work under its
 terms, with knowledge of his or her Copyright and Related Rights in the
 Work and the meaning and intended legal effect of CC0 on those rights.
 ․
 1. Copyright and Related Rights. A Work made available under CC0 may be
 protected by copyright and related or neighboring rights ("Copyright and
 Related Rights"). Copyright and Related Rights include, but are not
 limited to, the following:
 ․
   i. the right to reproduce, adapt, distribute, perform, display,
      communicate, and translate a Work;
  ii. moral rights retained by the original author(s) and/or performer(s);
 iii. publicity and privacy rights pertaining to a person's image or
      likeness depicted in a Work;
  iv. rights protecting against unfair competition in regards to a Work,
      subject to the limitations in paragraph 4(a), below;
   v. rights protecting the extraction, dissemination, use and reuse of data
      in a Work;
  vi. database rights (such as those arising under Directive 96/9/EC of the
      European Parliament and of the Council of 11 March 1996 on the legal
      protection of databases, and under any national implementation
      thereof, including any amended or successor version of such
      directive); and
 vii. other similar, equivalent or corresponding rights throughout the
      world based on applicable law or treaty, and any national
      implementations thereof.
 ․
 2. Waiver. To the greatest extent permitted by, but not in contravention
 of, applicable law, Affirmer hereby overtly, fully, permanently,
 irrevocably and unconditionally waives, abandons, and surrenders all of
 Affirmer's Copyright and Related Rights and associated claims and causes
 of action, whether now known or unknown (including existing as well as
 future claims and causes of action), in the Work (i) in all territories
 worldwide, (ii) for the maximum duration provided by applicable law or
 treaty (including future time extensions), (iii) in any current or future
 medium and for any number of copies, and (iv) for any purpose whatsoever,
 including without limitation commercial, advertising or promotional
 purposes (the "Waiver"). Affirmer makes the Waiver for the benefit of each
 member of the public at large and to the detriment of Affirmer's heirs and
 successors, fully intending that such Waiver shall not be subject to
 revocation, rescission, cancellation, termination, or any other legal or
 equitable action to disrupt the quiet enjoyment of the Work by the public
 as contemplated by Affirmer's express Statement of Purpose.
 ․
 3. Public License Fallback. Should any part of the Waiver for any reason
 be judged legally invalid or ineffective under applicable law, then the
 Waiver shall be preserved to the maximum extent permitted taking into
 account Affirmer's express Statement of Purpose. In addition, to the
 extent the Waiver is so judged Affirmer hereby grants to each affected
 person a royalty-free, non transferable, non sublicensable, non exclusive,
 irrevocable and unconditional license to exercise Affirmer's Copyright and
 Related Rights in the Work (i) in all territories worldwide, (ii) for the
 maximum duration provided by applicable law or treaty (including future
 time extensions), (iii) in any current or future medium and for any number
 of copies, and (iv) for any purpose whatsoever, including without
 limitation commercial, advertising or promotional purposes (the
 "License"). The License shall be deemed effective as of the date CC0 was
 applied by Affirmer to the Work. Should any part of the License for any
 reason be judged legally invalid or ineffective under applicable law, such
 partial invalidity or ineffectiveness shall not invalidate the remainder
 of the License, and in such case Affirmer hereby affirms that he or she
 will not (i) exercise any of his or her remaining Copyright and Related
 Rights in the Work or (ii) assert any associated claims and causes of
 action with respect to the Work, in either case contrary to Affirmer's
 express Statement of Purpose.
 ․
 4. Limitations and Disclaimers.
 ․
 a. No trademark or patent rights held by Affirmer are waived, abandoned,
    surrendered, licensed or otherwise affected by this document.
 b. Affirmer offers the Work as-is and makes no representations or
    warranties of any kind concerning the Work, express, implied,
    statutory or otherwise, including without limitation warranties of
    title, merchantability, fitness for a particular purpose, non
    infringement, or the absence of latent or other defects, accuracy, or
    the present or absence of errors, whether or not discoverable, all to
    the greatest extent permissible under applicable law.
 c. Affirmer disclaims responsibility for clearing rights of other persons
    that may apply to the Work or any use thereof, including without
    limitation any person's Copyright and Related Rights in the Work.
    Further, Affirmer disclaims responsibility for obtaining any necessary
    consents, permissions or other rights required for any use of the
    Work.
 d. Affirmer understands and acknowledges that Creative Commons is not a
    party to this document and has no duty or obligation with respect to
    this CC0 or use of the Work.
