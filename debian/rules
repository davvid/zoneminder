#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

ifeq ($(DEB_BUILD_ARCH_OS),hurd)
ARGS := -DZM_NO_MMAP=ON
endif

%:
	dh $@ --buildsystem=cmake --builddirectory=dbuild \
            --with sphinxdoc,apache2,linktree

override_dh_auto_configure:
	cp -arv RtspServer            dep/                   # Git submodule from MUT component.
	cp -arv Crud                  web/api/app/Plugin/    # Git submodule from MUT component.
	cp -arv CakePHP-Enum-Behavior web/api/app/Plugin/    # Git submodule from MUT component.
	dh_auto_configure -- $(ARGS)                       \
            -DCMAKE_VERBOSE_MAKEFILE=ON                    \
            -DCMAKE_BUILD_TYPE=Release                     \
            -DZM_CONFIG_DIR="/etc/zm"                      \
            -DZM_CONFIG_SUBDIR="/etc/zm/conf.d"            \
            -DZM_RUNDIR="/run/zm"                          \
            -DZM_SOCKDIR="/run/zm"                         \
            -DZM_TMPDIR="/tmp/zm"                          \
            -DZM_CGIDIR="/usr/lib/zoneminder/cgi-bin"      \
            -DZM_CONTENTDIR="/var/cache/zoneminder"        \
            -DZM_DIR_EVENTS="/var/cache/zoneminder/events" \
            -DZM_DIR_IMAGES="/var/cache/zoneminder/images" \
            -DZM_PATH_ARP="/bin/ip neigh"                  \
            -DZM_PATH_ZMS="/zm/cgi-bin/zms"                \
            -DZM_SYSTEMD="OFF"                             \
            -DZM_FONTDIR="/usr/share/zoneminder/www/fonts" \
        ;

override_dh_clean:
	dh_clean $(MANPAGES1)
	$(RM) -r docs/_build docs/installationguide web/api/app/Plugin/Crud dep/RtspServer

override_dh_auto_build-indep:
	$(MAKE) -C docs html
	dh_auto_build

MANPAGES1 = \
    dbuild/scripts/zmupdate.pl.1    \
    dbuild/scripts/zmaudit.pl.1     \
    dbuild/scripts/zmcamtool.pl.1   \
    dbuild/scripts/zmcontrol.pl.1   \
    dbuild/scripts/zmdc.pl.1        \
    dbuild/scripts/zmfilter.pl.1    \
    dbuild/scripts/zmpkg.pl.1       \
    dbuild/scripts/zmsystemctl.pl.1 \
    dbuild/scripts/zmtelemetry.pl.1 \
    dbuild/scripts/zmtrack.pl.1     \
    dbuild/scripts/zmtrigger.pl.1   \
    dbuild/scripts/zmvideo.pl.1     \
    dbuild/scripts/zmwatch.pl.1     \
    dbuild/scripts/zmx10.pl.1

$(MANPAGES1):
	# Generate man page(s):
	pod2man -s1 --stderr --utf8 $(patsubst %.1, %, $@) $@

override_dh_installman-arch: $(MANPAGES1)
	$(MAKE) -C docs man
	dh_installman --language=C $(MANPAGES1)

override_dh_auto_install:
	dh_auto_install --arch --destdir=$(CURDIR)/debian/tmp
	
	# Remove empty directories:
	-find $(CURDIR)/debian/tmp/usr -mindepth 1 -type d -empty -print0 | xargs -0r rm -vf
	
	# Don't ship core.php as it contains pre-generated/static salts. (#831984)
	# We keep core.php.default as a template for the postinst.
	rm -f $(CURDIR)/debian/tmp/usr/share/zoneminder/www/api/app/Config/core.php

override_dh_install:
	dh_install -XLICENSE -XLICENSE.txt -X.gitignore

override_dh_fixperms:
	dh_fixperms
	## 637685
	-chmod -c o-r $(CURDIR)/debian/zoneminder/etc/zm/zm.conf

override_dh_installinit:
	dh_installinit --no-start

override_dh_installsystemd:
	dh_installsystemd --no-enable --no-start

override_dh_apache2:
	dh_apache2 --noenable

override_dh_strip:
	dh_strip --dbgsym-migration='zoneminder-dbg (<< 1.29.0+dfsg-2~)'
